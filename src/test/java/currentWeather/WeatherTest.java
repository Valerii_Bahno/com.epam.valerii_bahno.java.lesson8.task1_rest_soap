package currentWeather;

import currentWeather.models.responses.getbycurrentweather.GetCurrentWeatherResponse;
import currentWeather.services.WeatherService;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class WeatherTest {

    @Test
    public void testGetByCityName() {
        //GIVEN
        String cityName = "Kyiv";

        //WHEN
        GetCurrentWeatherResponse actualCityName = new WeatherService().getByCityName(cityName);

        //THEN
        assertThat(actualCityName.getName()).isEqualTo(cityName);
        assertThat(actualCityName.getId()).isEqualTo(703448);
    }

    @Test
    public void testGetByCityId() {
        //GIVEN
        int cityId = 702550;

        //WHEN
        GetCurrentWeatherResponse actualCityId = new WeatherService().getByCityId(cityId);

        //THEN
        assertThat(actualCityId.getId()).isEqualTo(cityId);
        assertThat(actualCityId.getName()).isEqualTo("Lviv");
    }

    @Test
    public void testGetByGeoCoordinates() {
        //GIVEN
        double lat = 48.45;
        double lon = 34.98;

        //WHEN
        GetCurrentWeatherResponse actualGeoCoordinates = new WeatherService().getGeoCoordinates(lat, lon);

        //THEN
        assertThat(actualGeoCoordinates.getCoord().getLat()).isEqualTo(lat);
        assertThat(actualGeoCoordinates.getCoord().getLon()).isEqualTo(lon);
        assertThat(actualGeoCoordinates.getName()).isEqualTo("Dnipro");
    }

    @Test
    public void testGetByZipCode() {
        //GIVEN
        int zipCode = 77598;
        String countryCode = "US";

        //WHEN
        GetCurrentWeatherResponse actualZipCode = new WeatherService().getByZipCode(zipCode, countryCode);

        //THEN
        assertThat(actualZipCode.getSys().getCountry()).isEqualTo(countryCode);
        assertThat(actualZipCode.getName()).isEqualTo("Webster");
    }

}


