package currentWeather.services;

import currentWeather.models.responses.getbycurrentweather.GetCurrentWeatherResponse;
import io.restassured.RestAssured;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;

public class WeatherService {

    final String commonUrl = "https://api.openweathermap.org/data/2.5/weather?";
    final String nameCity = "q=";
    final String idCity = "id=";
    final String geoLat = "lat=";
    final String geoLon = "&lon=";
    final String zip = "zip=";
    final String appId = "&appid=";
    final String apiKey = "60084cd0aa8fce93c01bca45c296ed4e"; //- API_key after registering

    public GetCurrentWeatherResponse getByCityName(String cityName) {
        return RestAssured
                .given().header("Content-Type", ContentType.XML)
                .filters(new ErrorLoggingFilter(),
                        new RequestLoggingFilter(),
                        new ResponseLoggingFilter())
                .when().get(commonUrl + nameCity + cityName + appId + apiKey)
                .then().statusCode(HttpStatus.SC_OK).extract().as(GetCurrentWeatherResponse.class);
    }

    public GetCurrentWeatherResponse getByCityId(int cityId) {
        return RestAssured
                .given().header("Content-Type", ContentType.XML)
                .filters(new ErrorLoggingFilter(),
                        new RequestLoggingFilter(),
                        new ResponseLoggingFilter())
                .when().get(commonUrl + idCity + cityId + appId + apiKey)
                .then().statusCode(HttpStatus.SC_OK).extract().as(GetCurrentWeatherResponse.class);
    }

    public GetCurrentWeatherResponse getGeoCoordinates(double lat, double lon) {
        return RestAssured
                .given().header("Content-Type", ContentType.XML)
                .filters(new ErrorLoggingFilter(),
                        new RequestLoggingFilter(),
                        new ResponseLoggingFilter())
                .when().get(commonUrl + geoLat + lat + geoLon + lon + appId + apiKey)
                .then().statusCode(HttpStatus.SC_OK).extract().as(GetCurrentWeatherResponse.class);
    }

    public GetCurrentWeatherResponse getByZipCode(int zipCode, String countryCode) {
        return RestAssured
                .given().header("Content-Type", ContentType.XML)
                .filters(new ErrorLoggingFilter(),
                        new RequestLoggingFilter(),
                        new ResponseLoggingFilter())
                .when().get(commonUrl + zip + zipCode + "," + countryCode + appId + apiKey)
                .then().statusCode(HttpStatus.SC_OK).extract().as(GetCurrentWeatherResponse.class);
    }
}