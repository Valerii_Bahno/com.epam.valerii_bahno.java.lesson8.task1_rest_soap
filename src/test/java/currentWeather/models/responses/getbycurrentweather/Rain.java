package currentWeather.models.responses.getbycurrentweather;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Rain{

    @JsonProperty("1h")
    private double $1h;

    public double get1h(){
        return $1h;
    }

    @Override
    public String toString(){
        return
                "Rain{" +
                        "1h = '" + "1h" + '\'' +
                        "}";
    }
}