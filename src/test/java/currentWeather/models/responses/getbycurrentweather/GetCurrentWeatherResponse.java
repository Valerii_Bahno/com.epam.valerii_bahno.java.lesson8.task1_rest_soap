package currentWeather.models.responses.getbycurrentweather;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GetCurrentWeatherResponse {

	@JsonProperty("dt")
	private int dt;

	@JsonProperty("coord")
	private Coord coord;

	@JsonProperty("visibility")
	private int visibility;

	@JsonProperty("weather")
	private List<WeatherItem> weather;

	@JsonProperty("name")
	private String name;

	@JsonProperty("cod")
	private int cod;

	@JsonProperty("main")
	private Main main;

	@JsonProperty("clouds")
	private Clouds clouds;

	@JsonProperty("id")
	private int id;

	@JsonProperty("timezone")
	private int timezone;

	@JsonProperty("sys")
	private Sys sys;

	@JsonProperty("base")
	private String base;

	@JsonProperty("wind")
	private Wind wind;

	@JsonProperty("rain")
	private Rain rain;

	public int getDt(){
		return dt;
	}

	public Coord getCoord(){
		return coord;
	}

	public int getVisibility(){
		return visibility;
	}

	public List<WeatherItem> getWeather(){
		return weather;
	}

	public String getName(){
		return name;
	}

	public int getCod(){
		return cod;
	}

	public Main getMain(){
		return main;
	}

	public Clouds getClouds(){
		return clouds;
	}

	public int getId(){
		return id;
	}

	public int getTimezone(){
		return timezone;
	}

	public Sys getSys(){
		return sys;
	}

	public String getBase(){
		return base;
	}

	public Wind getWind(){
		return wind;
	}

	public Rain getRain(){
		return rain;
	}

	@Override
 	public String toString(){
		return 
			"GetByCityIdResponse2{" + 
			"dt = '" + dt + '\'' + 
			",coord = '" + coord + '\'' + 
			",visibility = '" + visibility + '\'' + 
			",weather = '" + weather + '\'' + 
			",name = '" + name + '\'' + 
			",cod = '" + cod + '\'' + 
			",main = '" + main + '\'' + 
			",clouds = '" + clouds + '\'' + 
			",id = '" + id + '\'' +
			",timezone = '" + timezone + '\'' +
			",sys = '" + sys + '\'' + 
			",base = '" + base + '\'' + 
			",wind = '" + wind + '\'' +
			",rain = '" + rain + '\'' +
			"}";
		}
}