package currentWeather.models.responses.getbygeocoordinates;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Main{

	@JsonProperty("temp")
	private double temp;

	@JsonProperty("feels_like")
	private double feelsLike;

	@JsonProperty("temp_min")
	private double tempMin;

	@JsonProperty("humidity")
	private int humidity;

	@JsonProperty("pressure")
	private int pressure;

	@JsonProperty("temp_max")
	private double tempMax;

	@JsonProperty("sea_level")
	private double seaLevel;

	@JsonProperty("grnd_level")
	private double grndLevel;

	public double getTemp(){
		return temp;
	}

	public double getFeelsLike(){
		return feelsLike;
	}

	public double getTempMin(){
		return tempMin;
	}

	public int getHumidity(){
		return humidity;
	}

	public int getPressure(){
		return pressure;
	}

	public double getTempMax(){
		return tempMax;
	}

	@Override
 	public String toString(){
		return 
			"Main{" + 
			"temp = '" + temp + '\'' + 
			",temp_min = '" + tempMin + '\'' +
			",feels_like = '" + feelsLike + '\'' +
			",humidity = '" + humidity + '\'' + 
			",pressure = '" + pressure + '\'' + 
			",temp_max = '" + tempMax + '\'' + 
			"}";
		}
}