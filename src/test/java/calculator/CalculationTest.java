package calculator;

import org.junit.Assert;
import org.junit.Test;
import org.tempuri.*;

public class CalculationTest {

    @Test
    public void testAdd() {
        AddResponse resultAdd = new AddResponse();
        resultAdd.setAddResult(120);
        Add add = new Add();
        add.setIntA(100);
        add.setIntB(20);
        Assert.assertEquals(resultAdd.getAddResult(), (add.getIntA() + add.getIntB()));
    }

    @Test
    public void testDivide() {
        DivideResponse resultDivide = new DivideResponse();
        resultDivide.setDivideResult(10);
        Divide divide = new Divide();
        divide.setIntA(50);
        divide.setIntB(5);
        Assert.assertEquals(resultDivide.getDivideResult(), (divide.getIntA() / divide.getIntB()));
    }

    @Test(expected = ArithmeticException.class)
    public void testDivideByZero() {
        Divide divide = new Divide();
        divide.setIntA(100);
        divide.setIntB(0);
        Assert.assertEquals(ArithmeticException.class, (divide.getIntA() / divide.getIntB()));
    }

    @Test
    public void testMultiply() {
        MultiplyResponse resultMultiply = new MultiplyResponse();
        resultMultiply.setMultiplyResult(2000);
        Multiply multiply = new Multiply();
        multiply.setIntA(50);
        multiply.setIntB(40);
        Assert.assertEquals(resultMultiply.getMultiplyResult(), (multiply.getIntA() * multiply.getIntB()));
    }

    @Test
    public void testSubtract() {
        SubtractResponse resultSubtract = new SubtractResponse();
        resultSubtract.setSubtractResult(100);
        Subtract subtract = new Subtract();
        subtract.setIntA(150);
        subtract.setIntB(50);
        Assert.assertEquals(resultSubtract.getSubtractResult(), (subtract.getIntA() - subtract.getIntB()));
    }
}
